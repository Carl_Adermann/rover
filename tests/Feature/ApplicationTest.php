<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\MissionControl;

class ApplicationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMove()
    {
        $missionControl = new MissionControl();
        $missionControl->landRovers();

        $this->assertTrue($missionControl->messages[0] == "1 3 N");
        $this->assertTrue($missionControl->messages[0] == "5 1 E");
    }
}
