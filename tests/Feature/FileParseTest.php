<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\FileParser;

class FileParseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParser()
    {

        $parser = new FileParser();

        //check valid characters and file size are accepted
        $parser->file = ["5 5",
                        "1 2 N",
                        "LMLMLMLMM",
                        "3 3 E",
                        "MMRMMRMRRM"];

        $this->assertFalse($parser->isMessageCorrupt());

        $parser = new FileParser();

        $parser->file = ["5 5",
                        "1 2 N",
                        "LMLMLMLMM",
                        "3 3 E",
                        "MMRMMRMRRM",
                        "1 3 E",
                        "MLRMRRM",
                        "7 4 E",
                        "MMMRRM"];

        $this->assertFalse($parser->isMessageCorrupt());

        $parser = new FileParser();

        $parser->file = ["5 5"];

        //check if an invalid file size isn't accepted
        $this->assertTrue($parser->isMessageCorrupt());


        $parser = new FileParser();

        $parser->file = ["A A",
                        "1 2 N",
                        "LMLMLMLMM",
                        "3 3 E",
                        "MMMRMRRM"];

        //check if an invalid characters aren't accepted
        $this->assertTrue($parser->isMessageCorrupt());


        $parser->file = ["5 5",
                        "1 2 N",
                        "lmlmlmlm",
                        "3 3 E",
                        "MMMRMRRM"];

        //check if an lower case characters aren't accepted
        $this->assertTrue($parser->isMessageCorrupt());

    }

    public function testRemoveWhiteSpace()
    {
        $parser = new FileParser();

        $removed = $parser->removeWhiteSpace('5 5  ');

        $this->assertTrue($removed[0] == "5");
        $this->assertTrue($removed[1] == "5");
    }

    public function testGetNumberOfLines()
    {
        $parser = new FileParser();

        $parser->file = ["5 5",
                        "1 2 N",
                        "lmlmlmlm",
                        "3 3 E",
                        "MMMRMRRM"];

        $numOfLines = $parser->getNumberOfLines();

        $this->assertTrue($numOfLines == 5);

    }

    public function testLocationRegex()
    {
        $parser = new FileParser();

        $line = "NESW";
        $accepted = $parser->isRegexFalse($line, FileParser::LOCATION_REGEX);

        $this->assertFalse($accepted);

        $line = "nesw";
        $accepted = $parser->isRegexFalse($line, FileParser::LOCATION_REGEX);

        $this->assertTrue($accepted);

        $line = "123w";
        $accepted = $parser->isRegexFalse($line, FileParser::LOCATION_REGEX);

        $this->assertTrue($accepted);
    }

    public function testMoveRegex()
    {
        $parser = new FileParser();

        $line = "LRM";
        $accepted = $parser->isRegexFalse($line, FileParser::MOVE_REGEX);

        $this->assertFalse($accepted);

        $line = "lrm";
        $accepted = $parser->isRegexFalse($line, FileParser::MOVE_REGEX);

        $this->assertTrue($accepted);

        $line = "123";
        $accepted = $parser->isRegexFalse($line, FileParser::MOVE_REGEX);

        $this->assertTrue($accepted);
    }

    public function testGridRegex()
    {
        $parser = new FileParser();

        $line = "123456";
        $accepted = $parser->isRegexFalse($line, FileParser::GRID_REGEX);

        $this->assertFalse($accepted);

        $line = "lrm";
        $accepted = $parser->isRegexFalse($line, FileParser::GRID_REGEX);

        $this->assertTrue($accepted);

        $line = "12lrm3";
        $accepted = $parser->isRegexFalse($line, FileParser::GRID_REGEX);

        $this->assertTrue($accepted);
    }
}
