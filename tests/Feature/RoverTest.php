<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Rover;
use App\Helpers\Location;

class RoverTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMove()
    {
        $location = new Location([0, 0, 'W']);

        $rover = new Rover([1, 1, 'E'], "LMLM");

        $this->assertTrue($rover->location->x == 1);
        $this->assertTrue($rover->location->y == 1);
        $this->assertTrue($rover->location->orientation == 'E');

        $rover->moveTo($location);

        $this->assertTrue($rover->location->x == 0);
        $this->assertTrue($rover->location->y == 0);
        $this->assertTrue($rover->location->orientation == 'W');
    }

    public function testNextLocation()
    {
        $rover = new Rover([1, 1, 'E'], "LMLM");

        $location = $rover->nextLocation('L');

        $this->assertTrue($location->x == 1);
        $this->assertTrue($location->y == 1);
        $this->assertTrue($location->orientation == 'N');

        $location = $rover->nextLocation('R');

        $this->assertTrue($location->x == 1);
        $this->assertTrue($location->y == 1);
        $this->assertTrue($location->orientation == 'S');

        $location = $rover->nextLocation('M');

        $this->assertTrue($location->x == 2);
        $this->assertTrue($location->y == 1);
        $this->assertTrue($location->orientation == 'E');
    }
}
