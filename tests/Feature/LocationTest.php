<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\Location;

class LocationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRotate()
    {

        $location = new Location([0, 0, 'E']);
        $location->rotateRight();

        $this->assertTrue($location->orientation == 'S');

        $location->rotateLeft();

        $this->assertTrue($location->orientation == 'E');

        //Assert after roating 4 times, the orientation will be at the original state
        $location->rotateLeft();
        $location->rotateLeft();
        $location->rotateLeft();
        $location->rotateLeft();
        $this->assertTrue($location->orientation == 'E');
    }

    public function testForward()
    {
        $location = new Location([0, 0, 'E']);
        $location->forward();

        $this->assertTrue($location->x == 1);
        $this->assertTrue($location->y == 0);

        $location = new Location([0, 0, 'N']);
        $location->forward();

        $this->assertTrue($location->x == 0);
        $this->assertTrue($location->y == 1);

        $location = new Location([1, 1, 'S']);
        $location->forward();

        $this->assertTrue($location->x == 1);
        $this->assertTrue($location->y == 0);

        $location = new Location([1, 1, 'W']);
        $location->forward();

        $this->assertTrue($location->x == 0);
        $this->assertTrue($location->y == 1);
    }
}
