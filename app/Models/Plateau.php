<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;

class Plateau extends Model
{
    //
    private $width;

    private $height;

    private $rovers;

    public function __construct($message)
    {
        parent::__construct();
        $this->width    = $message[0];
        $this->height   = $message[1];
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }
}
