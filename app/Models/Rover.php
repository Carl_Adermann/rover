<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
use App\Helpers\Location;

class Rover extends Model
{
    public $instructions;

    public $location;

    public function __construct($coordinates, $instructions)
    {
        parent::__construct();
        $this->location     = new Location($coordinates);
        $this->instructions = str_split($instructions[0]);
    }

    public function nextLocation($instruction)
    {
        $nextLocation = new Location ([$this->location->x, $this->location->y, $this->location->orientation]);
        
        switch ($instruction) {
            case 'R':
               $nextLocation->rotateRight();
            break;
            case 'L':
               $nextLocation->rotateLeft();
            break;
            case 'M':
                $nextLocation->forward();
            break;
            default;
                throw new Exception('Instructions are dirty '. $instruction);
        }
        
        return $nextLocation;
    }

    public function moveTo(Location $location)
    {
        $this->location = $location;
    }
    
}
