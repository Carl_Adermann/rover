<?php

namespace App\Helpers;
use File;
use Exception;
use App\Models\Rover;
use App\Models\Plateau;
use App\Helpers\FileParser;

class MissionControl
{

    const FILE_LOCATION = 'storage/message.txt';

    const MOVEMENT      = 'M';

    private $grid;

    private $rovers     = [];

    public $messages;

    public function landRovers()
    {
        $this->parser = new FileParser();

        //the Input message is sanitised here as well.
        $this->message = $this->parser->getInput();

        //Used to stop the rovers from exiting the plateau
        $this->plateau = new Plateau($this->message[0]);

        $this->setRovers();

        //Starts the rovers exploration
        $this->exploreMars();
    }

    private function setRovers()
    {
        $numOfLinesInFile = $this->parser->getNumberOfLines();

        //start at one to skip the first line which is contains the grid data
        for($i = 1; $i < $numOfLinesInFile; $i = $i + 2)
        {
            $coordinates    = $this->message[$i];
            $instructions   = $this->message[$i + 1];

            $this->rovers[] = new Rover($coordinates, $instructions);
        }
    }

    public function exploreMars()
    {
        foreach($this->rovers as $this->rover){
            $this->sendRoverExploring();
            $this->messages[] = $this->rover->location->getMessage();
        }
    }

    private function sendRoverExploring()
    {
        foreach($this->rover->instructions as $instruction){
            //the next location is needed so Mission Control can assert it won't lead the rover into a collision with another rover or off the plateau
            $nextLocation = $this->rover->nextLocation($instruction);

            $this->moveRoverToNextLocation($nextLocation, $instruction);
        }
    }

    private function moveRoverToNextLocation($nextLocation, $instruction)
    {
        //Check if the instruction is safe
        //Abort if the instruction will cause rovers to exit the plateau or if they rovers will collide with each other
        if($instruction == static::MOVEMENT){
            if(!$this->isMovementSafe($nextLocation)){
                throw new Exception("Movement not safe.");
            }
        }
        $this->rover->moveTo($nextLocation);
    }

    public function isMovementSafe($nextLocation)
    {
        $collision = $this->willMovementCollideWithOtherRovers($nextLocation);
        $inGrid = $this->willMovementExitGrid($nextLocation);

        return !$collision && $inGrid;
    }
    private function willMovementCollideWithOtherRovers($nextLocation)
    {
        foreach($this->rovers as $rover){
            if($rover->location->willCollide($nextLocation)){
                return true;
            }
        }
        return false;
    }

    private function willMovementExitGrid($nextLocation)
    {
        $inX =  $this->isLocationInGrid($nextLocation->x, $this->plateau->getWidth());
        $inY = $this->isLocationInGrid($nextLocation->y, $this->plateau->getheight());

        return $inX && $inY;
    }

    private function isLocationInGrid($coordinate, $gridLimit)
    {
        $withinLimit = $coordinate <= $gridLimit;
        $greaterThanOrigin = $coordinate >= 0;

        return $withinLimit && $greaterThanOrigin;
    }
}
