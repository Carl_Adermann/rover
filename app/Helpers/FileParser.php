<?php

namespace App\Helpers;
use File;
use Exception;
use App\Models\Rover;
use App\Models\Plateau;

class FileParser
{

    const FILE_LOCATION         = 'storage/message.txt';

    const LOCATION_REGEX        = "[^NESW|0-9|\s]";

    const MOVE_REGEX            = "[^LRM|\s]";

    const GRID_REGEX            = "[^0-9|\s]";

    private $message;

    public $file;

    public function getInput()
    {
        $this->file = file(static::FILE_LOCATION);

        if($this->isMessageCorrupt()){
            throw new Exception("Unaccepted characters found in input");
        } 

        $this->parseFile();

        return $this->message;
    }

    public function isMessageCorrupt()
    {
        if(!$this->isFileCorrectSize()){
            return true;
        }

        $numOfLinesInFile = $this->getNumberOfLines();

        if ($this->isRegexFalse($this->file[0], static::GRID_REGEX)){
            return true;
        }

        for($i = 1; $i < $numOfLinesInFile; $i = $i + 2)
        {
            if($this->isRegexFalse($this->file[$i], static::LOCATION_REGEX)){
                return true;
            }

            if($this->isRegexFalse($this->file[$i + 1], static::MOVE_REGEX)){
                return true;
            }
        }

        return false;
    }

    public function isRegexFalse($line, $accepted_regex)
    {
        preg_match("/" . $accepted_regex ."/", $line, $found);

        return sizeOf($found) != 0;
    }

    public function isFileCorrectSize()
    {
        $numOfLinesInFile = $this->getNumberOfLines();
        
        if(($numOfLinesInFile%2) != 1 || $numOfLinesInFile < 3){
            return false;
            //'Number of lines in input must be greater than 2, and an odd number
        }

        return true;
    }

    private function parseFile()
    {
        foreach($this->file as $line){
            $this->message[] = $this->removeWhiteSpace($line);
        }
    }

    public function removeWhiteSpace($line)
    {
        $trimmed = trim($line);

        return explode(' ', $trimmed);
    }

    public function getNumberOfLines()
    {
        return sizeof($this->file);
    }
}
