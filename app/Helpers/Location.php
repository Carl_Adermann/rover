<?php

namespace App\Helpers;

class Location
{
    public $x;

    public $y;

    public $orientation;

    const ORIENTATION_MAP   = [ 'N' => 0,
                                'E' => 1,
                                'S' => 2,
                                'W' => 3];

    const REVERSE_MAP = ['N', 'E', 'S', 'W'];

    public function __construct($coordinates)
    {
        $this->x            = $coordinates[0];
        $this->y            = $coordinates[1];
        $this->orientation  = $coordinates[2];
    }

    public function rotateLeft()
    {
        //Convert the orientation an interger so we can easily rotate it using subtraction
        $orientation = static::ORIENTATION_MAP[$this->orientation];

        $orientation--;

        if($orientation < 0){
            $orientation = 3;
        }
        //convert the integer back to a NESW character
        $this->orientation = static::REVERSE_MAP[$orientation];
    }

    public function rotateRight()
    {
        //Convert the orientation an interger so we can easily rotate it using addition
        $orientation = static::ORIENTATION_MAP[$this->orientation];

        $orientation++;

        if($orientation > 3){
            $orientation = 0;
        }

        //convert the integer back to a NESW character
        $this->orientation = static::REVERSE_MAP[$orientation];
    }

    public function forward()
    {
        switch ($this->orientation) {
                case 'N': //North
                    $this->y++;
                break;

                case 'E': //East
                    $this->x++;
                break;

                case 'S': //South
                    $this->y--;
                break;

                case 'W': //West
                    $this->x--;
                break;
            }

    }

    public function willCollide(Location $location)
    {
        return $location->x == $this->x && $location->y == $this->y;
    }

    public function getMessage()
    {
        return $this->x . " " . $this->y . " " . $this->orientation;
    }
}
